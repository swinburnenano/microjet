import io, os, re, argparse, shutil
import tkinter as tk
from tkinter import filedialog

#creates an empty array for the gcode data
gf_Array = []

Start_Section = ['; -------- Start Section Code ------------',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',';M98 P"startprint.g"',";test", ";test1", ";test2", '; -------- Start Section Code ------------']
End_Section = ['; -------- Stop Section Code ------------', ';M98 P"stopprint.g"',';M98 P"stopprint.g"',';M98 P"stopprint.g"',';M98 P"stopprint.g"',';M98 P"stopprint.g"', '; -------- Stop Section Code ------------']

#Parser File Validator
def isfile(path):
    if os.path.isfile(path):
        return path
    else:
        raise argparse.ArgumentTypeError(f"readable_dir:{path} is not a valid path")


#Arguments Parser Setup
parser = argparse.ArgumentParser(description="The following arguments are optionl arguments for automatuon")
parser.add_argument('-g', '--gcode', type=isfile, metavar='', help='Location Of The Gcode File with "" around it')
parser.add_argument('-s', '--starttemplate', type=isfile, metavar='', help='Location Of The Start Template File with "" around it')
parser.add_argument('-e', '--endtemplate', type=isfile, metavar='', help='Location Of The End Template File with "" around it')
parser.add_argument('-ss', '--startsection', type=isfile, metavar='', help='(OPTIONAL) Location Of The Start Section File With "" around it')
parser.add_argument('-es', '--endsection', type=isfile, metavar='', help='(OPTIONAL) Location Of The End Section File With "" around it')
args = parser.parse_args()

#startsection file Checker
if args.startsection != None:
    print("[INFO (Optional) - Custom Start Section File Dectected")
    Start_Section.clear()
    if ".txt" not in args.startsection.lower():
        #if the file isnt a .txt file the program exits
        print("[ERROR] - Not A Valid Start Section File (.txt)")
        input('[ACTION] - Press The ENTER Key To Exit')
        exit()
    else:
        Start_Section.append('; -------- Start Section Code ------------')
        with open(args.startsection) as startSectionFile:
            #iterates over start code file
            for line in startSectionFile:
                #adds each line to an array
                Start_Section.append(line)
        Start_Section.append('; -------- Start Section Code ------------')

#endsection file Checker
if args.endsection != None:
    print("[INFO] (Optional) - Custom End Section File Dectected")
    End_Section.clear()
    if ".txt" not in args.endsection.lower():
        #if the file isnt a .txt file the program exits
        print("[ERROR] - Not A Valid Start Section File (.txt)")
        input('[ACTION] - Press The ENTER Key To Exit')
        exit()
    else:
        End_Section.append('; -------- Stop Section Code ------------')
        with open(args.endsection) as endsectionFile:
                #iterates over start code file
                for line in endsectionFile:
                    #adds each line to an array
                    End_Section.append(line)
        End_Section.append('; -------- Stop Section Code ------------')

#Checks if the file parsed in is a .gcode File
def gcodeChkr(file):
    if ".gcode" not in file.lower():
        #if the file isnt a .gcode file the program exits
        print("[ERROR] - Not A Valid Gcode File (.gcode)")
        input('[ACTION] - Press The ENTER Key To Exit')
        exit()
    else:
        return True

#Checks if the file parsed in is a .txt File
def txtChkr(file):
    if ".txt" not in file.lower():
        #if the file isnt a .txt file the program exits
        print("[ERROR] - Not A Valid text File (.txt)")
        input('[ACTION] - Press The ENTER Key To Exit')
        exit()
    else:
        return True


#Checks if the gcode file has been parsed in by args. if not it will ask for it with the GUI
if args.gcode == None:
    #Asks for the file location (GUI INTERFACE)
    print("[INFO] - Please Open Your .GCODE File")
    Gcode_File = filedialog.askopenfilename()
    #Checks If Gcode File Is a .Gcode File
    if gcodeChkr(Gcode_File):
        #args.gcode = Gcode_File
        print("[INFO] - Gcode File Is Good")
else:
    gcodeChkr(args.gcode)
    Gcode_File = args.gcode


if args.starttemplate == None:
    #Asks for the file location (GUI INTERFACE)
    print("[INFO] - Please Open Your Start Template File (.txt)")
    Start_Code = filedialog.askopenfilename()
    #Checks If Gcode File Is a .txt File
    if txtChkr(Start_Code):
        #args.starttemplate = Start_Code
        print("[INFO] - Start Template File Is Good")
else:
    txtChkr(args.starttemplate)
    Start_Code = args.starttemplate

if args.endtemplate == None:
    #Asks for the file location (GUI INTERFACE)
    print("[INFO] - Please Open Your End Template File (.txt)")
    End_Code = filedialog.askopenfilename()
    #Checks If Gcode File Is a .txt File
    if txtChkr(End_Code):
        #args.endtemplate = End_Code
        print("[INFO] - End Template File Is Good")
else:
    #checks if the file is a valid txt file
    txtChkr(args.endtemplate)
    End_Code = args.endtemplate

root = tk.Tk()
root.withdraw()

#-------------------------- CORE MODIFYER -----------------
def modifycore(GF):
    #opens the Gcode File
    #Created a temp variable for all the changes to be made.
    tempvar = ""
    with open(GF) as my_fileGF:
        #iterates over start code file
        for line in my_fileGF:
            #adds each line to an array
            gf_Array.append(line)
    #loops through the array and detects / removes any lines containing a Z with a number next to it
    print("[INFO] - Finding Z's")
    print("[INFO] - Searching for any G1's / G0's that have no following commands")
    # IDX = Row number
    # X = Data
    for idx, x in enumerate(gf_Array):
        tempvar = x
        changes = 0

        #-------------- This Loop Removes Z's from the G1/G0 Arguments --------------------
        #
        if bool(re.search(r"(G1|G0)\s((Z-?\d*.?\d*)\s(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)\s*(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)*\s*(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)*|((X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)\s(Z-?\d*.?\d*)\s(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)*\s*(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)*)|((X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)\s(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)\s(Z-?\d*.?\d*)\s(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)*)|((X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)\s(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*)\s(X-?\d*.?\d*|Y-?\d*.?\d*|F-?\d*.?\d*))\s(Z-?\d*.?\d*)|(Z-?\d*.?\d*)\s)", tempvar)):
            #this detects Z's in a string.
            #THERE IS A BUG.where we put the + at the end of the regex you can't put a * becuase it will break.. idk its weird but this still works.
            # print(" REPL-Z Pre Line:",idx," Stuff: ", tempvar)
            tempvar = re.sub(r"\s?Z-?\d*.?\d+", "", tempvar)
            # print(" REPL-Z Post Line:",idx, " Stuff: ", tempvar)
            # print("-------------")
            changes = 1

        #This looks for any G1's Followed By F's
        if bool(re.search(r"G1\sF-?\d*.?\d+\n", tempvar)):
            # print(" REPL-G Pre Line:",idx," Stuff: ", tempvar)
            tempvar = re.sub(r"G1\s", "", tempvar)
            # print(" REPL-G Post Line:",idx, " Stuff: ", tempvar)
            # print("-------------")
            changes = 1

        #This looks for any lone G1/G0's
        if bool(re.search(r"^(G1|G0)\s*\n$", tempvar)):
            tempvar = ""
            changes = 1

        if changes == 1:
            gf_Array[idx] = tempvar

    #writes array to file
    with open(GF, 'w') as f:
        for item in gf_Array:
            f.write("%s" % item)

#------------------- Variable definitions --------------------
# GF = Gcode File location
# SC = Starting Code File Location
# EC = End Code File Location
# SLN = Starting Line (starting line for the starting code)
# ELN = End Line (starting line for the end of the code)
# SS = Start Script Code
# ES = End Script Code
# SSP = Start Section Position List
# ESP = End Section Position List
# SL = Start List for the S245
# EL = End List for the S245

#--------------------- START CODE INSERTER  ---------------------
def insertStartCode(GF, SC, SLN):
    #opens file in read mode
    f = open(GF, "r")
    #reads file into memory
    fcontents = f.readlines()
    f.close()

    #start code into an array
    sc_Array = []
    #uses the starting line number as a master index (so it can iterate by 1 to write the lines)
    index = SLN - 1

    #Opens start code file
    with open(SC) as my_file:
        #iterates over start code file
        for line in my_file:
            #adds each line to an array
            sc_Array.append(line)
        #adds a new line to the LAST item as it will not automatically do that
        sc_Array.append("\n")

        #iterates over the length of the starting code array
        sclen = len(sc_Array) -1
        for x in range(sclen,-1,-1):
            #places the Starting code array text after the start line of the first bit of code
            #note the loop starts from the last line of the text file and loops backward as if you did it forward your entire startup script
            #would be in reverse order.
            fcontents.insert(index, sc_Array[x])

    f = open(GF, "w")
    fcontents = "".join(fcontents)
    f.write(fcontents)
    f.close()
    print("[INFO] - Start Code Written To File")

#--------------------- END CODE INSERTER  ---------------------
def insertEndCode(GF, EC, ELN):
    #opens file in read mode
    f = open(GF, "r")
    #reads file into memory
    fcontents = f.readlines()
    f.close()

    #start code into an array
    ec_Array = []
    #uses the starting line number as a master index (so it can iterate by 1 to write the lines)
    index = ELN + 1

    #Opens start code file
    with open(EC) as my_file:
        #iterates over start code file
        for line in my_file:
            #adds each line to an array
            ec_Array.append(line)
        #adds a new line to the LAST item as it will not automatically do that
        ec_Array.append("\n")

        #iterates over the length of the starting code array
        eclen = len(ec_Array) -1
        for x in range(eclen,-1,-1):
            #places the Starting code array text after the start line of the first bit of code
            #note the loop starts from the last line of the text file and loops backward as if you did it forward your entire startup script
            #would be in reverse order.
            fcontents.insert(index, ec_Array[x])

    f = open(GF, "w")
    fcontents = "".join(fcontents)
    f.write(fcontents)
    f.close()
    print("[INFO] - End Code Written To File")

#--------------------- START CODE CHECKER  ---------------------
def checkforstartcode(GF):
    print("[INFO] - Looking For Start Code")
    StartCodeDetection = "Move to clearance level\n"
    #Used to open a file
    with open(GF) as myFile:
        #enumerates the lines in a file and gets each line number
        for startlinenum, line in enumerate(myFile, 1):
            #if the line number contains the StartCodeDetection string
            if StartCodeDetection in line:
                print('[INFO] - Starting Code Found on line ' + str(startlinenum))
                return startlinenum

    print("[ERROR] - Start of file not found :( ")
    exit()


#--------------------- END CODE CHECKER  ---------------------
def checkforendcode(GF):
    EndCodeDetection = "M2\n"
    #Used to open a file
    with open(GF) as myFile:
        #enumerates the lines in a file and gets each line number
        for endlinenum, line in reversed(list(enumerate(myFile))):
            #if the line number contains the EndCodeDetection string
            if EndCodeDetection in line:
                print('[INFO] - Ending Code Found on line ' + str(endlinenum))
                return endlinenum

    print("[ERROR] - End of file not found :( ")
    exit()



#----------------- START / END SECTION INSERTER --------------------
def startEndSection(SS, ES, GF):
    #opens file in read mode
    f = open(GF, "r")
    #reads file into memory
    fcontents = f.readlines()
    f.close()

    #runs the function getstartsections to retrieve the lines required
    SSP = getstartsections(GF)
    SSindexlength = len(SS) - 1
    #Loops through each of the found lines where the data needs to go
    for loopIndex, sspLine in enumerate(SSP, 1):
        # subtract 1 from the index  and get the length of the ammount of lines to add and multiply it by two
        #Every time you add new start code you are adding more lines to the file this means that the index for the next lines to add is out by the ammount you added.
        #loops through each piece of data that needs to be added
        for x in range(SSindexlength, -1, -1):
            StartNewlinepos = (loopIndex - 1) * (len(SS) * 2)
            fcontents.insert(sspLine + StartNewlinepos, "\n")
            fcontents.insert(sspLine + StartNewlinepos, SS[x])
            #Adds a new line at the end of each item in the row

    f = open(GF, "w")
    fcontents = "".join(fcontents)
    f.write(fcontents)
    f.close()
    print("[INFO] - Start Section Code Written To File")

    #opens file in read mode
    f = open(GF, "r")
    #reads file into memory
    fcontents = f.readlines()
    f.close()
#----------------
    #End Section Stuff
    ESP = getendsections(GF)
    ESindexlength = len(ES) - 1
    #Loops through each of the found lines where the data needs to go
    for loopIndex, espLine in enumerate(ESP, 1):
        #as you add lines to the array you need to reload the write head equal to the ammount of lines you add. e.g if you add 2x Start Section Lines, you must add 2xStartCodeLines length to the WriteLine
        for x in range(ESindexlength, -1, -1):
            EndNewlinepos = (loopIndex - 1) * (len(ES) * 2)
            fcontents.insert(espLine + EndNewlinepos, "\n")
            fcontents.insert(espLine + EndNewlinepos, ES[x])
            #Adds a new line at the end of each item in the row
    f = open(GF, "w")
    fcontents = "".join(fcontents)
    f.write(fcontents)
    f.close()
    print("[INFO] - Ending Section Code Written To File")

#----------------- START SECTION FINDER --------------------
def getstartsections(GF):
    startcode_Arr = []
    print("[INFO] - Looking For Start Sections")
    StartCodeDetection = "; plunge\n"
    #Used to open a file
    with open(GF) as myFile:
        #enumerates the lines in a file and gets each line number
        for startlinenum, line in enumerate(myFile, 1):
            #if the line number contains the StartCodeDetection string
            if StartCodeDetection in line:
                startcode_Arr.append(startlinenum)
    if not startcode_Arr:
        print ("[ERROR] - No Start Sections Found")
    else:
        print("[INFO] - Starting Sections Found!")
        return startcode_Arr
#----------------- END  SECTION FINDER --------------------
def getendsections(GF):
    endCode_Arr = []
    print("[INFO] - Looking For End Sections")
    endCodeDetection = "; Retract\n"
    #Used to open a file
    with open(GF) as myFile:
        #enumerates the lines in a file and gets each line number
        for endLineNum, line in enumerate(myFile, 1):
            #if the line number contains the StartCodeDetection string
            if endCodeDetection in line:
                endCode_Arr.append(endLineNum)
    if not endCode_Arr:
        print ("[ERROR] - No Ending Sections Found")
    else:
        print("[INFO] - Ending Sections Found!")
        return endCode_Arr

def backup(GF):
    src_dir = os.getcwd() #get the current working dir
    # create a dir where we want to copy and rename
    os.listdir()
    shutil.copy(GF, GF+".BACKUP")
    print("[INFO] - Backup Created with .BACKUP after it")

#------------------ ADD S254 ------------------
def addS245(SL, EL, GF):
    #opens file in read mode
    f = open(GF, "r")
    #reads file into memory
    fcontents = f.readlines()
    f.close()
    if len(SL) == len(EL):
        for idx, ans in enumerate(SL):
            SL1 = SL[idx] - 1
            EL1 = EL[idx] - 1
            for line in range(SL1,EL1):
                if bool(re.search(r'^G1.*',fcontents[line])):
                    dataoutput=fcontents[line].strip()+" S245"
                    fcontents[line] = dataoutput+"\n"
    f = open(GF, "w")
    fcontents = "".join(fcontents)
    f.write(fcontents)
    f.close()
    print("[INFO] - Done adding S245")

#---------- BEGIN CODE ---------
print("[INFO] ---------------------------------------------")
print("[INFO] |               PROGRAM START               |")
print("[INFO] ---------------------------------------------")
print ("[INFO] - ~~~~~~~~~~~~~~~Backup Original Gcode~~~~~~~~~~~~~~~")
#Backs Up ORIGINAL gcode file
backup(Gcode_File)
print ("[INFO] - ~~~~~~~~~~~~~~~~~~G-Code Cleanup~~~~~~~~~~~~~~~~~~~")
modifycore(Gcode_File)
#gets the start line number and adds the start code
print ("[INFO] - ~~~~~~~~~~~~~~~~Start / End Insert~~~~~~~~~~~~~~~~~")
startlinenum = checkforstartcode(Gcode_File)
insertStartCode(Gcode_File, Start_Code, startlinenum)
#gets the end line number and adds end line code
endlinenum = checkforendcode(Gcode_File)
insertEndCode(Gcode_File, End_Code, endlinenum)
#Adding S245 To Lines G1.
print ("[INFO] - ~~~~~~~~~~~~~~~~~~S245 Additions~~~~~~~~~~~~~~~~~~~")
addS245(getstartsections(Gcode_File), getendsections(Gcode_File), Gcode_File)
#Adding Start / End Section
print ("[INFO] - ~~~~~~~~~~~~~~~Start / Stop Sections~~~~~~~~~~~~~~~")
startEndSection(Start_Section, End_Section, Gcode_File)
print("[INFO] ---------------------------------------------")
print("[INFO] |               PROGRAM END                 |")
print("[INFO] ---------------------------------------------")

input('[ACTION] - Press The ENTER Key To Exit')
